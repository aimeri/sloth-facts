const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const dotenv = require('dotenv').config();
const mongoURI = process.env.MLAB_URI;

const MongoClient = require('mongodb').MongoClient;

MongoClient.connect(mongoURI,(err, client) => {
    if (err) return console.log(err);
    db = client.db('quotes');
    app.listen(3000,()=>{
    });
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.set('view engine', 'ejs');



app.get('/', (req, res)=>{
    db.collection('slothFacts').find().toArray((err, result) => {
        if (err) return console.log(err);
        res.render('index.ejs', {slothFacts: result});
        
    });

});

app.get('/add', (req, res)=>{
    db.collection('slothFacts').find().toArray((err, result) => {
        if (err) return console.log(err);
        res.render('add.ejs', {slothFacts: result});
        
    });

});

app.get('/documentation', (req, res)=>{
    db.collection('slothFacts').find().toArray((err, result) => {
        if (err) return console.log(err);
        res.render('documentation.ejs', {slothFacts: result});
        
    });

});

app.get('/facts', (req, res)=>{
    db.collection('slothFacts').find().toArray((err, results) => {
        res.send(results);
      });

});
app.get('/facts/count', (req, res)=>{
    db.collection('slothFacts').find().toArray((err, results) => {
        let resultsCount = 'There are ' + results.length + ' sloth facts available currently.';
        res.send(resultsCount);
      });

});
app.get('/random', (req, res)=>{
    db.collection('slothFacts').find().toArray((err, results) => {
        res.send(results[Math.floor(Math.random() * results.length)]);
      });

});

app.post('/sloth',(req, res)=>{
    db.collection('slothFacts').save(req.body, (err, result) => {
        if (err) return console.log(err);
        res.redirect('/');
    });
});